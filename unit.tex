\documentclass{mast}

\begin{document}

\maketitle

\section{Overview}

Perspectives is a philosophically heavy unit. It's more important, in the long run, to be able to detect ideas or problems that feel like Perspectives, rather than remembering every related theorem under the sun. Having good prototypes of Perspectives is crucial.

So what is Perspectives? It's as the name suggests --- problems that are easier to solve when you consider them from another perspective. Rather than just being about finding a systematic manner to solve a problem, Perspectives is about finding the right problem to solve.

Sometimes this means counting an equivalent quantity that is easier to analyze (Bijections). Other times this means realizing a complex system with many choices actually mostly consists of easily analyzable, independent choices and few, if any, constrained choices (Freedom). And still other times you want to cleverly group items together so something can be systematically counted.

\section{Bijections}

Here we discuss some well-known bijections and combinatorial identities.

\subsection{Stars and Bars}
Stars and Bars is the answer to the question ``how many ways can we give identical things to non-identical people?'' It's also known as sticks and stones or balls and urns. This is a clever trick often used in lower-level competition mathematics, and it's best thought of as a clever bijection.

\begin{theo}[Stars and Bars]
	The number of ways to distribute $n$ indistinguishable items to $k$ distinguishable people is $\binom{n+k-1}{k-1}$.
\end{theo}

\begin{pro}
	Let there be $k-1$ dividers and $n$ items in a line. Then we distribute the items between each set of dividers (and to the left of the leftmost divider and to the right of the rightmost divider) to the people in that order. Note that there are $\binom{n+k-1}{k-1}$ ways to do this, and this corresponds directly to the number of ways to directly distribute the items to the people.
	\[\star \star|\star|\star\star\star|\star\]
	\begin{center}
		\textit{Stars and Bars for $n=7$ and $k=4$.}
	\end{center}
\end{pro}

We present a fairly straightforward application of Stars and Bars with no restrictions.

\begin{exam}[AMC 10A 2003/21]
	Pat is to select six cookies from a tray containing only chocolate chip, oatmeal, and peanut butter cookies. There are at least six of each of these three kinds of cookies on the tray. How many different assortments of six cookies can be selected?
\end{exam}

\begin{sol}
	There are six stars and three bars, so the answer is $\binom{6+3-1}{3-1}=\binom{8}{2}=28$.
\end{sol}

You usually will have some sort of restrictions - most commonly, certain people must get a minimum of the distributed item. (This is evidenced by how hard it was for me to find an example for stars and bars with no restrictions.) In this case, allot the items "beforehand" and ignore the restrictions, while starting with less items than before you took care of the restrictions. This is all fairly abstract, so a concrete example will help.

\begin{exam}[AMC 8 2019/25]
	Alice has $24$ apples. In how many ways can she share them with Becky and Chris so that each of the three people has at least two apples?
\end{exam}

\begin{sol}
	We distribute $2$ apples to each of the $3$ people. So we have $18$ apples left and no more restrictions, so the answer is $\binom{18+3-1}{3-1}=\binom{20}{2}=190$.
\end{sol}

There will be times when you need to do another clever bijection to make stars and bars easier. Some example include having a limit on how many items people can receive, and having a number of items very close to this limit - in this case, we can think about distributing "negative items" - that is, how far away each person is from receiving the maximum.

\subsection{Choosing Unordered Elements}
Alternatively titled ascending numbers. We take a look at two generic examples that cover the section pretty well.

\begin{exam}[Ascending Numbers]
	An \textit{ascending number} is a number whose digits increase from left to right. How many four digit ascending numbers are there?
\end{exam}

\begin{sol}
	We choose four distinct digits between $1$ and $9$, and the order is fixed. Thus, the answer is just $\binom{9}{4}=126$.
\end{sol}

Now what if the number is just non-decreasing and can stay the same?

\begin{exam}[Non-descending Numbers]
	A \textit{nondescending number} is a number whose digits never decrease (but may stay the same) from left to right. How many four digit nondescending numbers are there?
\end{exam}

\begin{sol}
	Let the digits be $a,b,c,d$. Then we desire $1\leq a\leq b\leq c\leq d\leq 9$. But note this is also equivalent to $1\leq a<b+1<c+2<d+3\leq 12$. So we pick $4$ distinct numbers and match them with $a,b+1,c+2,d+3$. There are $\binom{9-1+4}{4}=\binom{12}{4}$ ways to do this, so the answer is $495$.
\end{sol}

This can also be done with stars and bars - if we let the "baskets" be the digits $1,2,\ldots, 9$ and the "stars" be the 4 digits we choose. We also get the value of $\binom{9-1+4}{4}=495$.

\subsection{Combinatorial Arguments}

This subsection is about interpreting algebraic sums combinatorially. We present some famous examples of combinatorial arguments, most famously the Hockey-Stick and Vandermonde identities.

We start with one of the most basic and fundamental examples of using a combinatorial argument to prove an algebraic identity, and contrast it with an algebraic approach.

\begin{exam}[Binomial Theorem]
	For any positive integer $n$, prove that $\sum\limits_{i=0}^n\binom{n}{i}=2^n$.
\end{exam}

\begin{sol}
	This is the number of ways to choose a subset of $n$ elements, as each possible size of subsets is covered.
\end{sol}

The algebraic solution involves noticing that $(1+1)^n$ is equivalent to the summation.

\begin{exam}[Committee Leader]
	For any positive integer $n$, prove that $\sum\limits_{i=0}^ni\binom{n}{i}=n2^{n-1}$.
\end{exam}

\begin{sol}
	The summation represents the number of ways to select a non-empty committee with a leader, as we first select the committee then choose the leader. But you can also choose the leader out of $n$ people and for the rest of the people, you can choose whether they are in the committee or not, so there are $n2^{n-1}$ ways to choose a committee with a leader.
\end{sol}

The fundamental idea is this: the binomial function $\binom{n}{k}$ can be interpreted as choosing $k$ people out of a group of size $n$. No matter how complex the identity, any combinatorial proof will use this.

You can also use an algebraic pairing argument to prove this identity.

\begin{exer}[Shift One]
	Prove, combinatorially and algebraically, that for positive integers $n$ and $k$,
	\[k\binom{n}{k}=n\binom{n-1}{k-1}.\]
\end{exer}

The meat of the subsection is the following two theorems: Vandermonde's and Hockey-Stick. Typically AIME problems that employ combinatorial arguments will utilize one of these two theorems, and it will often be in a very uncreative and straightforward manner.\footnote{Take, for instance, AIME I 2020/7 and AIME I 2015/12.}

\begin{theo}[Hockey-Stick]
	For positive integers $n,k$,
	\[\sum\limits_{i=k}^{n}\binom{i}{k}=\binom{n+1}{k+1}.\]

	\begin{center}
		\begin{asy}
			size(6cm);
			import olympiad;
			int chew(int n,int r){
					int res=1;
					for(int i=0;i<r;++i){
							res=quotient(res*(n-i),i+1);
						}
					return res;
				}
			for(int n=0;n<9;++n){
					for(int i=0;i<=n;++i){
							if((i==2 && n<8)||(i==3 && n==8)){
									if(n==8) {
											label(string(chew(n,i)),(11+n/2-i,-n),p=red+2.5);
										} else {
											label(string(chew(n,i)),(11+n/2-i,-n),p=blue+2);}
								} else {
									label(string(chew(n,i)),(11+n/2-i,-n));
								}
						}
				}
		\end{asy}

		\textit{Diagram from AoPS Wiki.}
	\end{center}
\end{theo}

The Hockey-Stick identity is named such because it looks like a hockey stick in Pascal's Triangle.

\begin{pro}
	Have a particle on the lattice grid starting at $(0,0)$, and allow it to either move $1$ unit right or $1$ unit up in each move. Then note that $\sum_{i=k}^{n}\binom{i}{k}$ is the sum of the number of ways to get to $(k,0),(k,1),\ldots,(k,n-k)$, and that $\binom{n+1}{k+1}$ is the number of ways to get to $(k+1,n-k)$. But note that to get to $(k+1,n-k)$, we go from a point $(k,i)$ to a point $(k+1,i)$ and then go straight up, which there is always exactly one way to do once you get to $(k,i)$. Thus the two values are equal.

	\begin{center}
		\begin{asy}
			import olympiad;
			size(5cm);
			int i, j;

			for(i=3; i<4; i=i+1) {
					for(j=0; j<5; j=j+1)
					dot((i,j));
				}

			draw((0,5)--(0,0)--(5,0));
			dot((3,0));
			label("$3,0$",(3,0),SW);
			dot((3,1));
			label("$3,1$",(3,1),SW);
			dot((3,2));
			label("$3,2$",(3,2),SW);
			dot((3,3));
			label("$3,3$",(3,3),SW);
			dot((3,4));
			label("$3,4$",(3,4),SW);
			dot((4,4));
			label("$4,4$",(4,4),SW);
		\end{asy}

		\textit{Example for $n=7$ and $k=3$.}
	\end{center}
\end{pro}

\begin{theo}[Vandermonde]
	For positive integers $m$, $n$, and $k$,
	\[\sum_{i=0}^k\binom {m}{i}\binom {n}{k-i}=\binom{m+n}{k}.\]
\end{theo}

\begin{pro}
	Note that this is the same as picking a committee of $k$ people from $m+n$ people, since for every committee, there is some arbitrary number $i$ such that we pick $i$ from the group of $m$ and the rest of the $k-i$ from the group of $n$.
\end{pro}

If you understand the proof of Vandermonde's, this problem should be very straightforward. If you don't, you will understand it after you solve this problem.

\begin{exer}[AIME I 2020/7]
	A club consisting of $11$ men and $12$ women needs to choose a committee from among its members so that the number of women on the committee is one more than the number of men on the committee. The committee could have as few as $1$ member or as many as $23$ members. Let $N$ be the number of such committees that can be formed. Find the sum of the prime numbers that divide $N$.
\end{exer}

\section{Pairing}

Sometimes counting problems will have a large degree of symmetry or regularity in its structure. This symmetry can be exploited by grouping (typically pairing) off elements.

\begin{exam}
	What is the probability that, after $5$ coins are flipped,there are more heads than tails?
\end{exam}

The answer is pretty intuitive: $\ansbold{\frac{1}{2}}$. That's because the probability of getting more heads is the same as the probability of geting more tails.

\begin{exam}
	What is the probability that, after $6$ coins are flipped, there are more heads than tails?
\end{exam}

Clearly the probability is not $\frac{1}{2}$. But the probability of getting more heads is still the probability of getting more tails. In this case, we just have to consider the probability we get the same number of heads as tails.

\begin{sol}
	Let the probability of getting more heads be $p$. Then by symmetry, the probability of getting more tails is also $p$.

	Note the only other possibility is that the number of heads is the same as the number of tails. This happens with a probability of $\frac{\binom{6}{3}}{2^6}=\frac{5}{16}$. Thus, $2p+\frac{5}{16}=1$, and $p=\ansbold{\frac{11}{32}}$.
\end{sol}

In this case, a pairing argument did not completely finish the problem, but what was left over was easy to deal with. This is a common pattern with pairing problems.

\begin{exer}[HMMT Feb. Guts 2011/22]
	Find the number of ordered triples $(a,b,c)$ of pairwise distinct integers such that $-31\leq a,b,c\leq 31$ and $a+b+c>0$.
\end{exer}

The idea is that the number of triples $(a,b,c)$ with $a+b+c>0$ is the same as with $a+b+c<0$. Thus, you just need to find the number of triples with $a+b+c=0$, which is much easier.

Pairing problems can sometimes take an algebraic taste. I wouldn't really consider them Perspectives problems, but do keep in mind they exist.

This next example is the epitome of pairing problems and leads perfectly to the next idea: the Principle of Inclusion-Exclusion.

\begin{exam}[AIME 1983/13]
	For $\{1, 2, 3, \ldots, n\}$ and each of its non-empty subsets a unique alternating sum is defined as follows. Arrange the numbers in the subset in decreasing order and then, beginning with the largest, alternately add and subtract successive numbers. For example, the alternating sum for $\{1, 2, 3, 6, 9\}$ is $9-6+3-2+1=5$ and for $\{5\}$ it is simply $5$. Find the sum of all such alternating sums for $n=7$.
\end{exam}

\begin{sol}
	Note that any subset $A$ not containing $7$ can be matched with a subset $B$ containing $7$, and further note that $S(A)+S(B)=7$. Since there are $2^6=64$ sets $A$ (we can treat the empty set as having alternating sum $0$), the answer is $64\cdot 7=\ansbold{448}$.
\end{sol}

\subsection{The Principle of Inclusion-Exclusion}

Refer to Appendix \ref{app:set-notation} if you do not know set notation.

Even though PIE problems may not invoke the idea of pairing, the proof of PIE itself does. Keep this in mind as you work through these problems.

You've probably heard of ``Venn Diagram'' problems before, and if you haven't, here's an example of it.

\begin{exam}
	$20$ students are taking Spanish and $30$ students are taking French. If everyone takes at least one language and there are $45$ total students, how many students are \emph{only} taking Spanish?
\end{exam}

\begin{sol}
	Let the amount of students taking Spanish and French be $x$. Then note that there are $20-x$ students only taking Spanish and $30-x$ students only taking French. We can add the students in all three of these groups to find the number of students taking at least one of Spanish or French. Since there are $45$ students, $20-x+30-x+x=45$, implying $50-x=45$ or $x=5$. Since we want the number of students taking only Spanish, our answer is $20-5=\ansbold{15}$.
\end{sol}

The Principle of Inclusion-Exclusion is about splitting the students into groups depending on the exact \emph{number} of classes they take (usually the exact classes they take are irrelevant), and making sure you count each student exactly once. We note that if we count everybody for each time they are in Spanish and each time they are in French, then we will "double-count" (count twice) the people in both Spanish and French. This means that we must subtract the people in both Spanish and French. Fortunately, this overcounting/undercounting behavior is actually quite predictable.

\begin{theo}[The Two-Set Case]
	For sets $A_1,A_2$,
	\[|A_1\cup A_2|=|A_1|+|A_2|-|A_1\cap A_2|.\]
\end{theo}

\begin{pro}
	Notice that you count the elements in \emph{exactly} one set once, but you count the elements in two sets twice. Thus, we must subtract the elements in both sets to account for this overcounting.
\end{pro}

Let's take a look at the case of $3$ sets.

\begin{theo}[The Three-Set Case]
	For sets $A_1,A_2,A_3$,
	\[|A_1\cup A_2\cup A_3| = |A_1|+|A_2|+|A_3|-|A_1\cap A_2|-|A_2\cap A_3|- |A_3\cap A_1|+|A_1\cap A_2\cap A_3|.\]
\end{theo}

\begin{pro}
	We note that if we count $A_1,A_2,A_3$ once, then we count everything in exactly two sets twice. Thus we subtract $|A_1\cap A_2|+|A_2\cap A_3|+|A_3\cap A_1|$. This means our current value is $|A_1|+|A_2|+|A_3|-|A_1\cap A_2|-|A_2\cap A_3|- |A_3\cap A_1|$, but we have counted every person in $|A_1|\cap |A_2|\cap |A_3|$ zero times. So we have to add $|A_1\cap A_2\cap A_3|$, giving us our final result as
	\[|A_1\cup A_2\cup A_3|=|A_1|+|A_2|+|A_3|-|A_1\cap A_2|-|A_2\cap A_3|- |A_3\cap A_1|+|A_1\cap A_2\cap A_3|.\]
\end{pro}

Here's a quick checkup on the three-set case of PIE.

\begin{exer}[AMC 10B 2017/13]
	There are $20$ students participating in an after-school program offering classes in yoga, bridge, and painting. Each student must take at least one of these three classes, but may take two or all three. There are $10$ students taking yoga, $13$ taking bridge, and $9$ taking painting. There are $9$ students taking at least two classes. How many students are taking all three classes?
\end{exer}

Keeping in mind how many times we count each number, we can generalize PIE. Let's say we have $n$ sets. Then we add the amount of terms in the individual sets, subtract the terms in $2$ sets, add the amount of terms in $3$ sets, and so on. Note that this means the amount of terms in \emph{at least} that many sets, not exactly.\footnote{If this confuses you, just think of the sum of the sizes of all intersections of $K$ sets.} The general rule is we add the amount of terms in $K$ sets if $K$ is odd and we subtract the amount of terms in $K$ sets if $K$ is even. (See the top of the section for a more formalized statement and proof.)

For example, with four sets $A_1,A_2,A_3,A_4$, we have
\[A_1\cup A_2\cup A_3\cup A_4=\sum\limits_{i=1}^{4}A_i-\sum\limits_{\text{sym}}A_1\cap A_2+\sum\limits_{\text{sym}}A_1\cap A_2\cap A_3-A_1\cap A_2\cap A_3 \cap A_4.\footnote{The summations with ``sym`` underneath just means all possible combinations of intersections of sets with a certain size. It is an abuse of notation.}\]

\begin{exer}
	Write PIE out for five sets.
\end{exer}

This exercise is optional, and you should only do it if you feel like you have a very weak grasp of PIE at this point.

You may have noticed a pattern here: we are adding intersections of an odd number of sets and subtracting intersections of an even number of sets. The natural question to ask is, ``Does this hold in general, and why?'' The answer to the first question is the general Principle of Inclusion-Exclusion, and the second can be answered with a pairing argument.

\begin{theo}[The Principle of Inclusion-Exclusion]
	For any set $\mathcal{A}$ whose elements are all sets themselves,
	\[\left|\bigcup_{S \in \mathcal{A}} S \right| = \sum_{\mathcal{A}' \subseteq \mathcal{A}} (-1)^{|\mathcal{A}'| + 1}\left|\bigcap_{S \in \mathcal{A}'} S\right|.\]
\end{theo}

Don't get boggled down by the notation. It just means that, over all combinations of intersections of sets $A_i$, its size is added if the number of sets in a certain combination is odd and subtracted if it is even. I strongly recommend you read \href{https://dennisc.net/pie.pdf}{my notes on PIE} for a more complete picture.

The following proof is very similar to AIME 1983/13, which was presented earlier in this section, and the main idea is proving each element gets counted exactly once. If you're more experienced, give proving it on your own a try.

\begin{pro}
	We prove that each element is counted exactly once. Say some element $X$ is in sets $S_1, S_2, \ldots, S_n$. For every combination of sets not containing $S_1$ with size $i$, there is a corresponding combination of sets containing $S_1$ with size $i+1$, as $S_1$ can just be inserted into the former combination. After accounting for $\{S_1\}$, which does not pair with anything, $X$ is counted exactly once as desired.
\end{pro}

\section{Freedom}

Consider the following problem.

\begin{exer}
	Griffin has ten shirts and seven pairs of pants. How many different combinations of shirts and pants can he wear?
\end{exer}

Solving this problem is very easy: you multiply the number of choices you have for each independent event. In this case, the answer is $10\cdot 7$. However, sometimes it isn't clear what the independent events are, or if there even are any at all. Thus, the goal of this section is to develop an intuition for transforming a combinatorial situation into a set of independent choices.

\begin{exer}
	How many ways can four coins be flipped so that an even number of heads come up?
\end{exer}

You might naively solve this problem by adding $\binom{4}{0}+\binom{4}{2}+\binom{4}{4}$, since there are $\binom{4}{0}$ ways to flip $0$ heads, et cetera. The answer should make you a little bit suspicious.

Solve this problem for five coins, six coins, and so on, until you see a pattern.

\begin{exam}
	How many ways can $n$ coins be flipped such that an even number of heads come up?
\end{exam}

At this point, you might suspect the answer is $2^{n-1}$. It seems that every coin added gives us another independent event with $2$ choices, but it's hard to see how this directly happens.

\begin{sol}
	Note that regardless of the first $n-1$ flips, there is exactly $1$ choice for the final flipped based on the parity of the number of heads in the previous $n-1$ flips.

	If the number of heads is odd, then the last flip must be heads, and if the number of heads is even, then the last flip must be tails.
\end{sol}

As a followup, how many ways can $n$ coins be flipped such that an odd number of heads come up? Think about why the numerical answers are the same.

In fact, this is how you prove the identity
\[\sum_{i=0}^{\lfloor\frac{n}{2}\rfloor}\binom{n}{2i}=\frac{2^n}{2}.\]
(Likewise for the odd version.) This is another example of how combinatorial arguments can be used to solve algebraic problems.

This next problem is a straightforward Freedom problem: the only thing you need to do is find the number of independent choices.

\begin{exam}[AMC 10A 2018/20]
	A scanning code consists of a $7 \times 7$ grid of squares, with some of its squares colored black and the rest colored white. There must be at least one square of each color in this grid of $49$ squares. A scanning code is called symmetric if its look does not change when the entire square is rotated by a multiple of $90 ^{\circ}$ counterclockwise around its center, nor when it is reflected across a line joining opposite corners or a line joining midpoints of opposite sides. What is the total number of possible symmetric scanning codes?
\end{exam}

\begin{sol}
	Two squares of the board must be identical if they have the same number.

	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|}
			\hline
			10 & 9 & 8 & 7 & 8 & 9 & 10 \\
			\hline
			9  & 6 & 5 & 4 & 5 & 6 & 9  \\
			\hline
			8  & 5 & 3 & 2 & 3 & 5 & 8  \\
			\hline
			7  & 4 & 2 & 1 & 2 & 4 & 7  \\
			\hline
			8  & 5 & 3 & 2 & 3 & 5 & 8  \\
			\hline
			9  & 6 & 5 & 4 & 5 & 6 & 9  \\
			\hline
			10 & 9 & 8 & 7 & 8 & 9 & 10 \\
			\hline
		\end{tabular}
	\end{center}

	Since there are $10$ numbers, there are $2^{10}$ colorings that maintain rotational and reflectional symmetry. However, we must subtract the all-black and all-white colorings of the board, so the answer is $2^{10}-2=\ansbold{1022}$.
\end{sol}

This final example is a bit different from all the others in that it is a States problem. The canonical reason I'm including it is to show how Perspectives can overlap with other topics, but it's really because I think the problem is cool.

\begin{exam}[Andy the Unicorn]
	Andy the unicorn is on a number line from $1$ to $2019$. He starts on $1$. Each step, he randomly and uniformly picks an integer greater than the integer he is currently on, and goes to it. He stops when he reaches $2019$. What is the probability he is ever on $1984$?
\end{exam}

\begin{sol}
	Note that Andy moves from the range $1-1983$ to the range $1984-2019$ exactly once, and furthermore, this is the only way for Andy to reach $1984$. Sincce the chance he reaches each number from $1984-2019$ is equal in this move, the answer is $\frac{1}{2019-1984+1}=36$.
\end{sol}

There are a couple derivatives of this problem in the States unit, if you are interested.

\section{MAT 2021/9}

In the spring of 2021, I wrote a problem with Aaron Guo that would eventually morph into MAT 2021/9. Here, we'll walk through an exploration of the problem and some related problems.

\begin{exam}[MAT 2021/9, initial]
	Alexander is juggling balls. At the start, he has four balls in his left hand: a red ball, a blue ball, a yellow ball, and a green ball. Each second, he tosses a ball from one hand to the other. If Alexander juggles for $20$ seconds and the number of ways for Alexander to toss the balls such that he has all four balls in the same hand at the end is $N$, find $N$.
\end{exam}

The problem isn't terribly hard. A naive states bash, which is the first thing I tried when I wrote this problem, managed to solve it. The Freedom argument is not hard to see either, so I strongly recommend you try it on your own before reading the solution.

\begin{sol}
	Notice that after every odd second, Alexander has $3$ balls in one hand and $1$ ball in the other. This means that, after $19$ seconds, Alexander has $3$ balls in one hand and $1$ ball in the other, and on the $20$th second he must throw the lone ball into the other hand.

	Since he can do whatever he wants for the first $19$ seconds, and there are $4$ ways to choose which ball to juggle, the answer is $4^{19}$.
\end{sol}

This initial version of the problem only incorporated Freedom and was not too interesting on its own. Fortunately, Aaron stumbled onto this variation (which would become the official problem) and came up with an ingenious pairing argument that convinced us all to use it.

\begin{exam}[MAT 2021/9]
	Alexander is juggling balls. At the start, he has four balls in his left hand: a red ball, a blue ball, a yellow ball, and a green ball. Each second, he tosses a ball from one hand to the other. If Alexander juggles for $20$ seconds and the number of ways for Alexander to toss the balls such that he has all four balls in his right hand at the end is $N$, find $N$.
\end{exam}

Notice that this time, all balls must end up in his right hand and cannot end in his left hand.

\begin{sol}
	Let the number of ways for Alexander to end with all balls in his left hand be $L$, and let it be $R$ for the right hand. Instead of trying to directly find $R$, we are going to find $L+R$ and $L-R$.

	We already found $L+R$ when solving the first draft of this problem: it's $4^{19}$ via a simple Freedom argument. Now let's turn our attention to $L-R$.

	Trying to find $L-R$ by finding $L$ and $R$ individually is a waste of time, since if we could've done that, we could just find $R$ on its own and call it a day. This means we're going to have to use some kind of pairing argument.

	Note that any set of tosses that ends with all balls in the right hand must reach the state $(2, 2)$ --- two balls in each hand. By symmetry the number of ways to reach $(0,4)$ --- all balls in the right hand --- is the same as the number of ways to reach $(4,0)$ while passing through $(2,2)$. This means that $L-R$ is the number of ways to $(4,0)$ without ever reaching $(2,2)$.

	On every even second, the state must be $(4,0)$, so Alexander can toss whichever of the $4$ balls he wants. On odd seconds, the state is $(3,1)$, so he only has one choice: pass the ball to the left. Since there are $10$ odd seconds, $L-R=4^{10}$.

	Subtracting $L-R$ from $L+R$ gives $2R=4^{19}-4^{10}=2^{38}-2^{20}$, or $R=\ansbold{2^{37}-2^{19}}$.
\end{sol}

An alternate solution I found half a year after the contest is quite similar.
\begin{enumerate}
	\item Find the number of ways to get to $(2,2)$ for the first time after $2k$ seconds.
	\item Find the number of ways to get from $(2,2)$ to $(0,4)$ in the remaining $20-2k$ seconds. (Hint: By symmetry, it is half of the number of ways to get to $(4,0)$ or $(0,4)$.)
	\item Sum it up for $1\leq k\leq 9$.
\end{enumerate}

This is my favorite Perspectives\footnote{Arguably it could be generating functions or states. That's what I love about this problem: there are so many ways to solve it.} problem because it combines two ideas into one: Freedom for $L+R$ and Pairing for $L-R$, and you need to have the meta-understanding of Perspectives to come up with this in the first place.

This idea of finding $L+R$ and $L-R$ can show up in some unexpected places. With this context, try the following problem.

\begin{exer}
	Let $S=\{\frac{1}{2},\frac{1}{3},\ldots,\frac{1}{100}\}$ and for any non-empty set $X$, let $f(X)$ be the product of the elements of $X$. Find $\sum f(X)$, where $X$ spans all of the subsets of $S$ with an odd number of elements.
\end{exer}

\problems

\appendix

\section{Set Notation}

\label{app:set-notation}

We define the intersection and union of a pair of sets, and show how this can be extended to multiple sets.

\begin{defi}[Intersection]
	Given two sets $A$ and $B$, their intersection $A\cap B$ is the set of all elements in both sets.
\end{defi}

\begin{defi}[Union]
	Given two sets $A$ and $B$, their union $A\cap B$ is the set of all elements in at least one of the two sets.
\end{defi}

As an example, consider the sets $\{1,2\}$ and $\{1,3\}$. Then their intersection would be $\{1\}$ and their union would be $\{1,2,3\}$.

There's a very handy trick to remember which symbol corresponds to the intersection and which corresponds to the union: the $\cup$ looks like a U, and union also begins with U. Thus, by process of elimination, the intersection symbol is $\cap$.

We can chain together intersections and unions for more than two sets. Try the following exercise yourself.

\begin{exer}
	Find $\{1,3,5\}\cap \{1,3,4\} \cap \{2,3,5\}$ and $\{1,3,5\}\cup \{1,3,4\}\cup \{2,3,5\}$.\footnote{If you're the type to be bothered by this, assume the order of operations is from left to right.}
\end{exer}

It turns out that the intersection of multiple sets is the set of all elements in all of the sets, and the union is the set of all elements in at least one of the sets. The order of operations also does not matter as long as \emph{you only have intersections or unions}.

\begin{exer}
	Prove that $S_1\cap S_2\cap S_3\cap \cdots \cap S_n$ is the set of all elements in all the sets, and prove that $S_1\cup S_2\cup S_3\cup \cdots \cup S_n$ is the set of all elements in at least one of the sets.\footnote{To prove that two sets $A$ and $B$ are identical, it suffices to prove that every element of $A$ is contained in $B$ and every element of $B$ is contained in $A$.}
\end{exer}
\end{document}
